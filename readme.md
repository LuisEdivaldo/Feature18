# Sura Android #

## For a new branch: ##

* For features: feature/#GitLabIssueID for example feature/#1
* For bugs: bug/#GitLabIssueID for example bug/#1
* For refactors: refactor/#GitLabIssueID for example refactor/#1
* For releases: releases release/RELEASE_CANDIDATE_VERSION for example release/1.0.0

## For commits: ##

"#GitLabIssueID: Message" for example: "#1: Example message"

***
***

## File Naming ##

### Class files ###
*  Class names are written in UpperCamelCase.
*  For classes that extend an Android component, the name of the class should end with the name of the component; for example: SignInActivity, SignInFragment, ImageUploaderService, ChangePasswordDialog.

### Drawable files ###

*  Drawable file names are written in lowercase_underscore.

**Naming conventions for drawables:**

| Asset Type | Prefix | Example |
| --- | --- | --- |
| Action bar | ab_ | ab_stacked.9.png |
| Button | btn_ | btn_send_pressed.9.png |
| Dialog | dialog_ | dialog_top.9.png |
| Divider | divider_ | divider_horizontal.9.png |
| Icon | ic_ | ic_star.png |
| Menu | menu_ | menu_submenu_bg.9.png |
| Notification | notification_ | notification_bg.9.png |
| Tabs | tab_ | tab_pressed.9.png |


**Naming conventions for icons:**

| Asset Type | Prefix | Example |
| --- | --- | --- |
| Icons | ic_ | ic_star.png |
| Launcher icons | ic_launcher | ic_launcher_calendar.png |
| Menu icons and Action Bar icons | ic_menu | ic_menu_archive.png |
| Status bar icons | ic_stat_notify | ic_stat_notify_msg.png |
| Tab icons | ic_tab | ic_tab_recent.png |
| Dialog icons | ic_dialog | ic_dialog_info.png |


**Naming conventions for selector states:**

| State | Suffix | Example |
| --- | --- | --- |
| Normal | _normal | btn_order_normal.9.png |
| Pressed | _pressed | btn_order_pressed.9.png |
| Focused | _focused | btn_order_focused.9.png |
| Disabled | _disabled | btn_order_disabled.9.png |
| Selected | _selected | btn_order_selected.9.png |


### Layout files ###

*  Layout file names are written in lowercase_underscore.
*  Layout files should match the name of the Android components that they are intended for but moving the top level component name to the beginning. For example, if we are creating a layout for the SignInActivity, the name of the layout file should be activity_sign_in.xml.

| Component | Class Name | Layout Name |
| --- | --- | --- |
| Activity | UserProfileActivity | activity_user_profile.xml |
| Fragment | SignUpFragment | fragment_sign_up.xml |
| Dialog | ChangePasswordDialog | dialog_change_password.xml |
| AdapterView item | --- | item_person.xml |
| Partial layout | --- | partial_stats_bar.xml |


*  Note that there are cases where these rules will not be possible to apply. For example, when creating layout files that are intended to be part of other layouts. In this case you should use the prefix partial_.

### Menu files ###

*  Similar to layout files, menu files should match the name of the component. For example, if we are defining a menu file that is going to be used in the UserActivity, then the name of the file should be activity_user.xml
*  A good practice is to not include the word menu as part of the name because these files are already located in the menu directory.

### Values files ###

*  Resource files in the values folder should be plural, e.g. strings.xml, styles.xml, colors.xml, dimens.xml, attrs.xml

## XML Naming ##

**ID naming**

*  IDs should be prefixed with the name of the element in lowercase underscore. For example:

| Element | Prefix |
| --- | --- |
| TextView | text_ |
| ImageView | image_ |
| Button | button_ |
| Menu | menu_ |

**Strings**

*  String names start with a prefix that identifies the section they belong to. For example registration_email_hint or registration_name_hint. If a string doesn't belong to any section, then you should follow the rules below:

| Prefix | Description |
| --- | --- | --- |
| error_ | An error message |
| msg_ | A regular information message |
| title_ | A title, i.e. a dialog title |
| action_ | An action such as "Save" or "Create" |

## Annotation styles ##

**Classes, Methods and Constructors**

* When annotations are applied to a class, method, or constructor, they are listed after the documentation block and should appear as one annotation per line.

```
@AnnotationA
@AnnotationB
public class MyAnnotatedClass { }
```

**Fields**

*  Annotations applying to fields should be listed on the same line, unless the line reaches the maximum line length.

`@Nullable @Mock DataManager dataManager;`

## String constants, naming, and values ##

*  Many elements of the Android SDK such as SharedPreferences, Bundle, or Intent use a key-value pair approach so it's very likely that even for a small app you end up having to write a lot of String constants.
*  When using one of these components, you must define the keys as a static final fields and they should be prefixed as indicated below.

| Element | Field Name Prefix |
| --- | --- | --- |
| SharedPreferences | PREF_ |
| Bundle | BUNDLE_ |
| Fragment Arguments | ARGUMENT_ |
| Intent Extra | EXTRA_ |
| Intent Action | ACTION_ |

*  Note that the arguments of a Fragment - Fragment.getArguments() - are also a Bundle. However, because this is a quite common use of Bundles, we define a different prefix for them.

## Arguments in Fragments and Activities ##

*  When data is passed into an Activity or Fragment via an Intent or a Bundle, the keys for the different values must follow the rules described in the section above.
*  When an Activity or Fragment expects arguments, it should provide a public static method that facilitates the creation of the relevant Intent or Fragment.
*  In the case of Activities the method is usually called getStartIntent():

```
public static Intent getStartIntent(Context context, User user) {
    Intent intent = new Intent(context, ThisActivity.class);
    intent.putParcelableExtra(EXTRA_USER, user);
    return intent;
}
```

* For Fragments it is named newInstance() and handles the creation of the Fragment with the right arguments:

```
public static UserFragment newInstance(User user) {
    UserFragment fragment = new UserFragment();
    Bundle args = new Bundle();
    args.putParcelable(ARGUMENT_USER, user);
    fragment.setArguments(args);
    return fragment;
}
```

*  Note 1: These methods should go at the top of the class before onCreate().
*  Note 2: If we provide the methods described above, the keys for extras and arguments should be private because there is no need for them to be exposed outside the class.

## Extras ##

**Don't Ignore Exceptions**
*  Do not write code ignoring exceptions

**Don't Catch Generic Exception**
*  Do not write code with generic exceptions

**Fully Qualify Imports**
*  Use explicit importing of package.

**Use Javadoc Standard Comments**
*  Write comments to classes and methods

**Define Fields in Standard Places**
*  Define fields either at the top of the file or immediately before the methods that use them.

**Limit Variable Scope**
*  Keep the scope of local variables to a minimum.

**Follow Field Naming Conventions**
*  Public static final fields (constants) are ALL_CAPS_WITH_UNDERSCORES.
*  Other fields start with a lowercase letter. (do NOT use prefixes like: mPrivateField/sStaticField)

**Use Standard Brace Style**
*  Braces do not go on their own line; they go on the same line as the code before.

**Use Standard Java Annotations**
*  Android standard practices for the predefined annotations (Deprecated, Override, SuppressWarnings)

**Treat Acronyms as Words**
*  Treat acronyms and abbreviations as words in naming variables, methods, and classes to make names more readable.

**Use TODO Comments**
*  Use TODO comments for code that is temporary, a short-term solution, or good-enough but not perfect.

**Log Sparingly**
*  Use Log only in necessary cases. Use the logging methods provided by the Log class to print out error messages or other information that may be useful for developers to identify issues: