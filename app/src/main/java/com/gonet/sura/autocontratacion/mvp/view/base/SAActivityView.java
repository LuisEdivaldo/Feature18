package com.gonet.sura.autocontratacion.mvp.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.app.FragmentManager;
import android.widget.Toast;

import com.gonet.sura.autocontratacion.activity.base.SAActivityBase;

import java.lang.ref.WeakReference;

/**
 * Created by Antonio on 06/03/18.
 */

public class SAActivityView<T extends SAActivityBase> {
    private WeakReference<T> activityRef;
    private ProgressDialog progressDialog;

    public SAActivityView(T activity) {
        activityRef = new WeakReference<>(activity);
    }

    @Nullable
    public T getActivity() {
        return activityRef.get();
    }

    @Nullable
    public Context getContext() {
        return getActivity();
    }

    @Nullable
    public FragmentManager getFragmentManager() {
        T activity = getActivity();
        return (activity != null) ? activity.getFragmentManager() : null;
    }

    public void showMessage(@StringRes int message) {
        final T activity = getActivity();
        if (activity == null) {
            return;
        }
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public void showMessage(String message) {
        final T activity = getActivity();
        if (activity == null) {
            return;
        }
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    public void showProgress(String tittle, String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            dissmissProgress();
        }

        final T activity = getActivity();
        if (activity == null) {
            return;
        }
        progressDialog = ProgressDialog.show(activity, tittle, message);
    }

    public void dissmissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
