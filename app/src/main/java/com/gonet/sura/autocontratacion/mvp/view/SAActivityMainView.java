package com.gonet.sura.autocontratacion.mvp.view;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentManager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.activity.SAActivityMain;
import com.gonet.sura.autocontratacion.fragment.SAFragmentHiringGreeting;
import com.gonet.sura.autocontratacion.mvp.view.base.SAActivityView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAActivityMainView extends SAActivityView<SAActivityMain> {

    @BindView(R.id.activity_main_content)
    FrameLayout contentFrameLayout;
    @BindView(R.id.pb_nombreProgres)
    ProgressBar progressBar;
    @BindView(R.id.ly_progress_Bar)
    LinearLayout layoutProgress;
    @BindView(R.id.rl_banner_img)
    RelativeLayout bannerImg;


    private FragmentManager fragmentManager;
    private SAFragmentHiringGreeting saFragmentHiringGreeting;

    public SAActivityMainView(SAActivityMain activity) {
        super(activity);
    }

    public void init() {

        ButterKnife.bind(this, getActivity());
        contentFrameLayout.requestFocus();
        saFragmentHiringGreeting = new SAFragmentHiringGreeting();
        fragmentManager = getActivity().getFragmentManager();
        fragmentManager.beginTransaction().add(R.id.activity_main_content, saFragmentHiringGreeting).commit();
    }

    @SuppressLint("ResourceType")
    public void fragmentChange(Fragment fragment) {

        fragmentManager.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.activity_main_content, fragment).addToBackStack(null).commit();
    }

    public void showIncreasedProgressBar(int advance){
        switch (advance){
            case 1:
                progressBar.setProgress(12);
                break;
            case 2:
                progressBar.setProgress(34);
                break;
            case 3:
                progressBar.setProgress(50);
                break;
            case 4:
                progressBar.setProgress(68);
                break;
            case 5:
                progressBar.setProgress(90);
                break;
            case 6:
                progressBar.setProgress(100);
                break;

        }

    }

    public void showProgressBar(boolean showProgress) {
        layoutProgress.setVisibility(showProgress ? View.VISIBLE : View.GONE);
    }


    public boolean onBackPress(){
        saFragmentHiringGreeting = (SAFragmentHiringGreeting) getFragmentManager().findFragmentById(R.id.activity_main_content);
        return saFragmentHiringGreeting.isVisible();
    }
}
