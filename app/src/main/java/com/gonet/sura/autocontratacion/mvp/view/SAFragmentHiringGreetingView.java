package com.gonet.sura.autocontratacion.mvp.view;

import android.graphics.Paint;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.fragment.SAFragmentHiringGreeting;
import com.gonet.sura.autocontratacion.mvp.view.base.SAFragmentView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAFragmentHiringGreetingView extends SAFragmentView<SAFragmentHiringGreeting> {

    private Handler handler;

    public SAFragmentHiringGreetingView ( SAFragmentHiringGreeting fragment )
    {
     super(fragment);
    }

    @BindView(R.id.fragment_hiring_greeting_name)
    public TextInputLayout nameTextInputLayout;


    @BindView(R.id.fragment_hiring_greeting_greeting_message)
    public TextView greetingMessageTextView;


    @BindView(R.id.fragment_hiring_greeting_start)
    public Button startButton;


    @BindView(R.id.fragment_hiring_greeting_privacy_notice)
    public TextView privacyNoticeTextView;




    public void init() {
        ButterKnife.bind(this, getActivity());

        startButton.setEnabled(false);

        privacyNoticeTextView.setPaintFlags(privacyNoticeTextView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        handler = new Handler(Looper.getMainLooper());

        nameTextInputLayout.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                handler.removeCallbacks(changeTextMessage);
                handler.postDelayed(changeTextMessage, 500);
            }
        });

        nameTextInputLayout.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus) {
                    greetingMessageTextView.setText(R.string.msg_Want_to_meet_yo);
                }
            }
        });
    }

    private final Runnable blankTextMessage = new Runnable() {
        @Override
        public void run() {
            greetingMessageTextView.setText("");
        }
    };

    private final Runnable changeTextMessage = new Runnable() {
        @Override
        public void run() {

            startButton.setEnabled(true);
            startButton.setBackgroundResource(R.drawable.btn_active);

            greetingMessageTextView.setText(R.string.msg_Pleasure);

            handler.postDelayed(blankTextMessage, 3000);
        }
    };

    public void removeCallBack(){
        handler.removeCallbacks(changeTextMessage);
        handler.removeCallbacks(blankTextMessage);
    }

}
