package com.gonet.sura.autocontratacion.mvp.view.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.widget.Toast;

import com.gonet.sura.autocontratacion.activity.base.SAActivityBase;
import com.gonet.sura.autocontratacion.fragment.base.SAFragmentBase;

import java.lang.ref.WeakReference;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAFragmentView<T extends SAFragmentBase> {
    private WeakReference<T> fragmentRef;
    private ProgressDialog progressDialog;

    protected SAFragmentView(T fragment) {
        fragmentRef = new WeakReference<>(fragment);
    }


    @Nullable
    public T getFragment() {
        return fragmentRef.get();
    }

    @Nullable
    public SAActivityBase getActivity() {
        return (SAActivityBase) fragmentRef.get().getActivity();
    }

    @Nullable
    public Context getContext() {
        return getActivity();
    }

    public void showMessage(@StringRes int message) {
        final T fragment = getFragment();
        if (fragment == null) {
            return;
        }
        Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showMessage(String message) {
        final T fragment = getFragment();
        if (fragment == null) {
            return;
        }
        Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showProgress(String tittle, String message) {
        if (progressDialog != null && progressDialog.isShowing()) {
            dissmissProgress();
        }

        final T fragment = getFragment();
        if (fragment == null) {
            return;
        }
        progressDialog = ProgressDialog.show(fragment.getActivity(), tittle, message);
    }

    public void dissmissProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }
}
