package com.gonet.sura.autocontratacion.utils.interfaces;

import android.view.View;

/**
 * Created by admincam on 12/03/18.
 */

public interface SAInterfaceAdapterToFragment {

   void showButtonsObjetives (View view);

}
