package com.gonet.sura.autocontratacion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceAdapterToFragment;

import butterknife.BindView;

/**
 * Created by Antonio on 05/03/18.
 */

public class SAAdapterListObjectives extends BaseAdapter implements SAInterfaceAdapterToFragment {

    int [] assets;
    String [] category;
    LayoutInflater inflater;
    Context context;
    SAInterfaceAdapterToFragment saInterfaceAdapterToFragment ;


    @BindView(R.id.rl_btns_counter)
    public RelativeLayout relativeLayoutButtons;
    @BindView(R.id.img_btn_less)
    public ImageButton imgBtnLess;
    @BindView(R.id.img_btn_more)
    public ImageButton imgBtnMore;
    @BindView(R.id.tv_accountant_objetives)
    public TextView textViewCount;

    public SAAdapterListObjectives(Context context, int[] assets, String[] category) {
        this.assets = assets;
        this.category = category;
        this.context = context;
    }

    @Override
    public int getCount() {
        return category.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        ImageView img;
        TextView txt;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.saadapter_list_objectives,parent,false);

        img = itemView.findViewById(R.id.adapter_list_objectives_img);
        img.setImageResource(assets[position]);
        txt = itemView.findViewById(R.id.adapter_list_objectives_txt);
        txt.setText(category[position]);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(view.getContext(), "Pulse aqui",Toast.LENGTH_SHORT).show();
                showButtonsObjetives(view);
            }
        });

        return itemView;
    }

    @Override
    public void showButtonsObjetives(View view) {

        if (view.getVisibility() == View.GONE){
            view.setVisibility(view.VISIBLE);
        }
        if(view.getVisibility() == view.VISIBLE){
            view.setVisibility(view.GONE);
        }



    }
}
