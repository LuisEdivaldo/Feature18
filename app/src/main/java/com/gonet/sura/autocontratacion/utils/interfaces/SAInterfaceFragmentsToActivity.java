package com.gonet.sura.autocontratacion.utils.interfaces;

import android.app.Fragment;



/**
 * Created by Antonio on 05/03/18.
 */

public interface SAInterfaceFragmentsToActivity {
    void fragmentChange(Fragment fragment);
    void showProgressBar(boolean showProgress);
    void showIncreasedProgressBar(int advance);
}
