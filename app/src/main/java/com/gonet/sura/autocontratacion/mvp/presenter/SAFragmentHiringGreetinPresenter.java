package com.gonet.sura.autocontratacion.mvp.presenter;

import com.gonet.sura.autocontratacion.mvp.view.SAFragmentHiringGreetingView;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAFragmentHiringGreetinPresenter {
    private SAFragmentHiringGreetingView view;

    public SAFragmentHiringGreetinPresenter(SAFragmentHiringGreetingView view)
    {
        this.view = view;
    }

    public void init() {
        view.init();

    }

    public void removeCallBack() {
        view.removeCallBack();
    }
}
