package com.gonet.sura.autocontratacion.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.fragment.base.SAFragmentBase;
import com.gonet.sura.autocontratacion.mvp.presenter.SAFragmentObjectivesPresenter;
import com.gonet.sura.autocontratacion.mvp.view.SAFragmentObjectivesView;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceAdapterToFragment;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceFragmentsToActivity;

import butterknife.ButterKnife;
import butterknife.OnItemClick;
import butterknife.Unbinder;


public class SAFragmentObjectives extends SAFragmentBase {

    private SAFragmentObjectivesPresenter presenter;

    private Unbinder unbinder;

    SAInterfaceFragmentsToActivity saInterfaceFragmentsToActivity;
    SAInterfaceAdapterToFragment saInterfaceAdapterToFragment;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a = null;

        if (context instanceof Activity)
            a=(Activity) context;
        try {
          saInterfaceFragmentsToActivity = (SAInterfaceFragmentsToActivity) a;
          //saInterfaceAdapterToFragment = (SAInterfaceAdapterToFragment) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement listMovements_listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater,container,savedInstanceState);
        View view = inflater.inflate(R.layout.safragment_objectives,null);

        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        presenter = new SAFragmentObjectivesPresenter(new SAFragmentObjectivesView(this));
        presenter.init();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @OnItemClick(R.id.fragment_objetives_listView)
    public void onClickButon(){

    }




}
