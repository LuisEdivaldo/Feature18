package com.gonet.sura.autocontratacion.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.fragment.base.SAFragmentBase;
import com.gonet.sura.autocontratacion.mvp.presenter.SAFragmentHiringGreetinPresenter;
import com.gonet.sura.autocontratacion.mvp.view.SAFragmentHiringGreetingView;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceFragmentsToActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public final class SAFragmentHiringGreeting extends SAFragmentBase {

    private SAFragmentHiringGreetinPresenter presenter;

    private Unbinder unbinder;

    SAInterfaceFragmentsToActivity saInterfaceFragmentsToActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity a = null;

        if (context instanceof Activity)
            a=(Activity) context;

        try {
            saInterfaceFragmentsToActivity = (SAInterfaceFragmentsToActivity) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement listMovements_listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.safragment_hiring_greeting, null);
        unbinder = ButterKnife.bind(this,view);

        return view;
    }

    @Override

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        presenter = new SAFragmentHiringGreetinPresenter(new SAFragmentHiringGreetingView(this));
        presenter.init();

    }

    @Override
    public void onStop() {
        super.onStop();
        presenter.removeCallBack();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.fragment_hiring_greeting_start)
    protected void onClickStart() {
        saInterfaceFragmentsToActivity.fragmentChange(new SAFragmentObjectives());
        saInterfaceFragmentsToActivity.showIncreasedProgressBar(1);
        saInterfaceFragmentsToActivity.showProgressBar(true);
    }

}
