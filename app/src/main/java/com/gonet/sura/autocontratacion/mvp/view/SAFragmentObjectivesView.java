package com.gonet.sura.autocontratacion.mvp.view;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.adapters.SAAdapterListObjectives;
import com.gonet.sura.autocontratacion.fragment.SAFragmentObjectives;
import com.gonet.sura.autocontratacion.mvp.view.base.SAFragmentView;
import com.gonet.sura.autocontratacion.utils.Constants;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceAdapterToFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAFragmentObjectivesView extends SAFragmentView<SAFragmentObjectives>{

    @BindView(R.id.fragment_objetives_listView)
    public ListView objetives_listview;

    SAAdapterListObjectives saAdapterListObjectives;
    SAInterfaceAdapterToFragment saInterfaceAdapterToFragment;


    public SAFragmentObjectivesView(SAFragmentObjectives fragment) {
        super(fragment);
    }

    public void init(){
        ButterKnife.bind(this, getActivity());


        saAdapterListObjectives = new SAAdapterListObjectives(getContext(), Constants.ASSETS,getContext().getResources().getStringArray(R.array.objectives));
        objetives_listview.setAdapter(saAdapterListObjectives);
        objetives_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               saAdapterListObjectives.showButtonsObjetives(view);

            }
        });

    }


}
