package com.gonet.sura.autocontratacion.utils;

import com.gonet.sura.autocontratacion.R;

/**
 * Created by Antonio on 05/03/18.
 */

public class Constants {
    public static int [] ASSETS = {
            R.drawable.ic_incidentals,
            R.drawable.ic_home,
            R.drawable.ic_education,
            R.drawable.ic_retirement,
            R.drawable.ic_patrimony,
            R.drawable.ic_other
    };
}
