package com.gonet.sura.autocontratacion.mvp.view;

import android.widget.TabHost;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.fragment.SAFragmentTabHost;
import com.gonet.sura.autocontratacion.mvp.view.base.SAFragmentView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Antonio on 08/03/18.
 */

public class SAFragmentTabHostView extends SAFragmentView<SAFragmentTabHost>{

    @BindView(R.id.th_objectives)
    public TabHost th_objectives;

    public SAFragmentTabHostView(SAFragmentTabHost fragment) {
        super(fragment);
    }



    public void init(){

        ButterKnife.bind(this, getActivity());

    }

    public void thObjectives(int tapOne, String title) {
        th_objectives.setup();
        TabHost.TabSpec spec;

        switch (tapOne) {

            case 1:
                spec = th_objectives.newTabSpec(title);
                spec.setContent(R.id.rl_tabGeneral);
                spec.setIndicator(title,getActivity().getResources().getDrawable(R.drawable.ic_close));
                th_objectives.addTab(spec);

                //general
                break;
            case 2:
                //educacion
                break;
            case 3:
                //retiro
                break;

        }



        //Tab 2
        spec = th_objectives.newTabSpec("Tab Two");
        spec.setContent(R.id.tab2);
        spec.setIndicator("Tab Two");
        th_objectives.addTab(spec);

    }
}
