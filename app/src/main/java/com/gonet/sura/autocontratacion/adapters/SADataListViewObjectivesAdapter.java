package com.gonet.sura.autocontratacion.adapters;

import android.view.View;
import android.widget.Toast;

/**
 * Created by Antonio on 05/03/18.
 */

public class SADataListViewObjectivesAdapter{
    private String Category;
    private int icon;

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }


}
