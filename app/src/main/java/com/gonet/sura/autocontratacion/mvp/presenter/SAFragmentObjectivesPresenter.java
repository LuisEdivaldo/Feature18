package com.gonet.sura.autocontratacion.mvp.presenter;

import com.gonet.sura.autocontratacion.mvp.view.SAFragmentObjectivesView;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAFragmentObjectivesPresenter {
    private SAFragmentObjectivesView view;

    public SAFragmentObjectivesPresenter(SAFragmentObjectivesView view){this.view = view;}

    public void init () {
    view.init();
    }
}
