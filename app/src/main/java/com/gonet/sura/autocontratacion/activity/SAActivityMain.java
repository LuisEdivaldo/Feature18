package com.gonet.sura.autocontratacion.activity;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.gonet.sura.autocontratacion.R;
import com.gonet.sura.autocontratacion.activity.base.SAActivityBase;
import com.gonet.sura.autocontratacion.mvp.presenter.SAActivityMainPresenter;
import com.gonet.sura.autocontratacion.mvp.view.SAActivityMainView;
import com.gonet.sura.autocontratacion.utils.interfaces.SAInterfaceFragmentsToActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class SAActivityMain extends SAActivityBase implements SAInterfaceFragmentsToActivity {
    private SAActivityMainPresenter presenter;

    private Unbinder unbinder;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        unbinder = ButterKnife.bind(this);
        presenter = new SAActivityMainPresenter(new SAActivityMainView(this));
        presenter.init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbinder.unbind();
    }




    public void fragmentChange(Fragment fragment) {
        presenter.fragmentChange(fragment);
    }

    @Override
    public void showProgressBar(boolean showProgress) {
        presenter.showProgressBar(showProgress);
    }

    @Override
    public void showIncreasedProgressBar(int advance) {
        presenter.showIncreasedProgressBar(advance);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (presenter.onBackPress())
        presenter.showProgressBar(false);

    }
}
