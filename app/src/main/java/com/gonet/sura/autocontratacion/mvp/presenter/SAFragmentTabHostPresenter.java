package com.gonet.sura.autocontratacion.mvp.presenter;

import com.gonet.sura.autocontratacion.mvp.view.SAFragmentTabHostView;

/**
 * Created by Antonio on 08/03/18.
 */

public class SAFragmentTabHostPresenter {

    private  SAFragmentTabHostView view;

    public SAFragmentTabHostPresenter (SAFragmentTabHostView view){this.view=view;}

    public void init() {
        view.init();

    }
}
