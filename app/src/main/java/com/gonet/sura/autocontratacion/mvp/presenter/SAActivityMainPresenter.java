package com.gonet.sura.autocontratacion.mvp.presenter;

import android.app.Fragment;

import com.gonet.sura.autocontratacion.mvp.view.SAActivityMainView;

/**
 * Created by Antonio on 07/03/18.
 */

public class SAActivityMainPresenter {
    private SAActivityMainView view;


    public SAActivityMainPresenter(SAActivityMainView view)
    {
        this.view = view;
    }

    public void init() {
        view.init();

    }

    public void fragmentChange(Fragment fragment) {
        view.fragmentChange(fragment);
    }

    public void showIncreasedProgressBar(int advance){
        view.showIncreasedProgressBar(advance);
    }

    public void showProgressBar(boolean showProgress){
        view.showProgressBar(showProgress);
    }

    public boolean onBackPress(){
        return  view.onBackPress();
    }

}
